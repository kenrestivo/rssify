# rssify

Tool for rssifying ogg files saved as YY-MM-DD-show_name.ogg

## Why??

Because RSS is cool.

It's usually run from a cron job in the middle of the night.

## Requirements
* vorbis-tools (ogg123, oggenc) version 1.4.0-6ubuntu1 or later, with this patch:
https://bugzilla.redhat.com/show_bug.cgi?id=1185558
or this one:
https://git.xiph.org/?p=vorbis-tools.git;a=commit;h=514116d7bea89dad9f1deb7617b2277b5e9115cd

## Installation

	lein bin

Create the installation file.

## Usage

  ./rssify config.edn

## Options

TODO: document the config file options

```clojure

{:in-oggs-path "/some/temp/borkenoggs"
 :out-oggs-path "/some/temp/tmp/"
 :cmd-path "/usr/local/bin/thrashcat"
 :backup-dir "/home/backups"
 :out-commands-file "/some/temp/get-me"}
```

## Examples

...

### Bugs

Actually, none at the moment, I'm sure some will crop up at some point.



## License

Copyright © 2014-2018 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
