(ns rssify.core
  (:gen-class)
  (:import [adamb.vorbis VorbisCommentHeader VorbisIO CommentField CommentUpdater])
  (:require [me.raynes.conch :refer [programs with-programs let-programs] :as csh]
            [me.raynes.conch.low-level :as sh]
            [taoensso.timbre.appenders.core :as appenders]
            [utilza.repl :as urepl]
            [clojure.tools.trace :as trace]
            [useful.map :as um]
            [rssify.feed :as feed]
            [utilza.java :as ujava]
            [clojure.edn :as edn]
            [clojure.java.io :as jio]
            [utilza.file :as file]
            [utilza.misc :as umisc]
            [taoensso.timbre :as log]
            [clojure.string :as st]))





;; IMPORTANT: This bare exec is here to dothis FIRST before running anything, at compile time
(log/merge-config! {:output-fn (partial log/default-output-fn {:stacktrace-fonts {}})
                    :level :info
                    :appenders {:println nil ;;(appenders/println-appender {:enabled? false})
                                :spit (appenders/spit-appender
                                       {:fname "rssify.log4j"})}})


;; IMPORTANT: enables the very very awesome use of clojure.tools.trace/trace-vars , etc
;; and logs the output of those traces to whatever is configured for timbre at the moment!
(alter-var-root #'clojure.tools.trace/tracer (fn [_]
                                               (fn [name value]
                                                 (log/debug name value))))





(defn process-config
  "Read and deserialize the config"
  [config-path]
  (->> config-path
       slurp
       edn/read-string))



(defn revision-info
  "Utility for determing the program's revision."
  []
  (let [{:keys [version revision]} (ujava/get-project-properties "rssify" "rssify")]
    (format "Version: %s, Revision %s" version revision)))

(defn run-all
  [config-path]
  (log/info "Loading config file " config-path)
  (let [conf (process-config config-path)]
    (run-all! conf)
    (log/info "concatenation done")
    (feed/make-feed! conf)
    (log/info "feed done")))

(defn -main [config-path & args]
  (log/info "Welcome to Rssify " (revision-info))
  (run-all config-path)
  (System/exit 0))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment

  core
  (run-all "resources/test-config.edn")

  (clojure.tools.trace/trace-vars parse)

  (log/info "test")
  
  (log/set-level! :trace)



  )

